﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomacaoTesteAvenueCode
{
    class DriverFactory

    {

        public static IWebDriver INSTANCE { get; set; } = null;



        public static void CreateInstance()

        {

            string browser = ConfigurationManager.AppSettings["BROWSER_DEFAULT"];



            if (INSTANCE == null)

            {

                switch (browser)

                {

                    case "chrome":

                        INSTANCE = new ChromeDriver();

                        INSTANCE.Manage().Window.Maximize();

                        break;

                    case "firefox":

                        INSTANCE = new FirefoxDriver();

                        INSTANCE.Manage().Window.Maximize();

                        break;

                    case "ie":

                        INSTANCE = new InternetExplorerDriver();

                        INSTANCE.Manage().Window.Maximize();

                        break;

                    default:

                        throw new Exception("This browser" + browser + "not found");

                }

            }

        }





        public static void QuitInstance()

        {

            INSTANCE.Quit();

            INSTANCE = null;

        }
    }
}