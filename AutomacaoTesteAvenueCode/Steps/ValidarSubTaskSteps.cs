﻿using AutomacaoTesteAvenueCode.Pages;
using System;
using TechTalk.SpecFlow;

namespace AutomacaoTesteAvenueCode
{
    [Binding]
    public class ValidarSubTaskSteps
    {
        ValidaButtonAdd tela = new ValidaButtonAdd();

        [When(@"possuo tarefa criada")]
        public void WhenPossuoTarefaCriada()
        {
            tela.ValidaTableDeTasks();
        }
        
        [When(@"aciono comando Manage SubTasks")]
        public void WhenAcionoComandoManageSubTasks()
        {
            tela.ValidaComandoManagerSubTask();
        }
        
        [When(@"abre popup de edicao da task")]
        public void WhenAbrePopupDeEdicaoDaTask()
        {
            tela.ValidaPopUpVisivel();
        }
        
        [When(@"escrevo o nome da SubTask no campo correto")]
        public void WhenEscrevoONomeDaSubTaskNoCampoCorreto()
        {
            tela.ValidaDescriptionSubTask();
        }
        
        [When(@"aciono como Add")]
        public void WhenAcionoComoAdd()
        {
            tela.ValidaAddSubTask();
        }
        
        /*[When(@"preencho os campos de informacao (.*) (.*) Aberto")]
        public void WhenPreenchoOsCamposDeInformacaoAberto(string p0, string p1)
        {
            tela.ValidaDescriptionSubTask();
            tela.ValidaAddSubTask();
            tela.ValidaDateSubTask();
            tela.ValidaAddSubTask();
        }*/
        
        [When(@"aciono comando add")]
        public void WhenAcionoComandoAdd()
        {
            tela.ValidaAddSubTask();
        }
        
        [When(@"aciono o comando sub task description")]
        public void WhenAcionoOComandoSubTaskDescription()
        {
            tela.ValidaDescriptionSubTask();
        }
        
        [When(@"ter o modal aberto")]
        public void WhenTerOModalAberto()
        {
            tela.ValidaPopUpVisivel();
        }
        
        [When(@"alterar o nome da task principal")]
        public void WhenAlterarONomeDaTaskPrincipal()
        {
            tela.ValidaNomeTaskModal();
        }
        
        [When(@"salvar")]
        public void WhenSalvar()
        {
            tela.ValidaAddSubTask();
        }
        
        [When(@"aciono o comando da sub taks description")]
        public void WhenAcionoOComandoDaSubTaksDescription()
        {
            tela.ValidaComandoManagerSubTask();
            tela.ValidaPopUpVisivel();
            tela.ValidaDescriptionSubTask();
            tela.ValidaAddSubTask();
        }
        
        [Then(@"consigo acionar comando de gerenciar SubTask")]
        public void ThenConsigoAcionarComandoDeGerenciarSubTask()
        {
            tela.ValidaComandoManagerSubTask();
        }
        
       /* [Then(@"botao mostra quantidade de subtask criadas")]
        public void ThenBotaoMostraQuantidadeDeSubtaskCriadas()
        {
           // ScenarioContext.Current.Pending();
        }*/
        
        [Then(@"subTask criada com sucesso")]
        public void ThenSubTaskCriadaComSucesso()
        {
           tela.ValidaListSubTask();
        }

        /*[Then(@"tenho (.*) conform tabela abaixo")]
        public void ThenTenhoConformTabelaAbaixo(string p0)
        {
           // ScenarioContext.Current.Pending();
        }*/

        /*[Then(@"sub task nao criada, nao pode alterar nome de task principal")]
        public void ThenSubTaskNaoCriadaNaoPodeAlterarNomeDeTaskPrincipal()
        {
           // ScenarioContext.Current.Pending();
        }*/

        [Then(@"salvar")]
        public void ThenSalvar()
        {
            tela.ValidaAddSubTask();
        }

        [Then(@"sub task criada com sucesso")]
        public void SubTaskCriadaComSucesso()
        {
           tela.ValidaListSubTask();
        }
        
        [Then(@"aparece na lista de sub tasks")]
        public void ThenApareceNaListaDeSubTasks()
        {
            tela.ValidaListSubTask();
        }
    }
}
