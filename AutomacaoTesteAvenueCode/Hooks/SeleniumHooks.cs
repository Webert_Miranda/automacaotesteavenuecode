﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace AutomacaoTesteAvenueCode.Hooks
{
   
        [Binding]

        class SeleniumHooks

        {

            [BeforeScenario]

            public void BeforeScenario()

            {

                DriverFactory.CreateInstance();

                DriverFactory.INSTANCE.Navigate().GoToUrl(ConfigurationManager.AppSettings["URL_DEFAULT"]);

            }

            [AfterScenario]

            public void AfterScenario()

            {

                DriverFactory.QuitInstance();

            }



        }
}
