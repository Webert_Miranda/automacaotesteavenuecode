﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomacaoTesteAvenueCode.Bases
{
    class Class1
    {
        #region Constructor and Objects

        protected WebDriverWait wait { get; set; }

        protected IWebDriver driver { get; set; }

        protected IJavaScriptExecutor javaScript { get; set; }



        //contrutor da pagina.

        public Class1()

        {

            PageFactory.InitElements(DriverFactory.INSTANCE, this);

            wait = new WebDriverWait(DriverFactory.INSTANCE, TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["TIMEOUT_DEFAULT"])));

            driver = DriverFactory.INSTANCE;

            javaScript = (IJavaScriptExecutor)driver;

        }

        #endregion



        #region Custom Actions



        protected bool IsElementPresent(By by)

        {

            try

            {

                driver.FindElement(by);

                return true;

            }

            catch (NoSuchElementException)

            {

                return false;

            }

        }

        protected void Click(IWebElement element, string text)

        {



        }



        protected void SendKeys(IWebElement element, string text)

        {



        }



        protected void ClickJavaScript(IWebElement element)

        {

            javaScript.ExecuteScript("arguments[0].click();", element);

        }

        #endregion
    }
}
