﻿Feature: AcessarAppTODO
	Background: 
	 Given esteja logado na tela do AppTODo
Scenario: Adicionar nova Task
	When insiro nome da task
	And  aciono o comando de mais
	Then nova task criada

Scenario: Excluir task
	When  acione o comando remove
	Then  task excluida

Scenario Outline: Validar campo de nome da task
	When inserir nome da task no campo de descricao <nome> 
	Then permite criacao de nova task <Resultado>
	And  valida quantidade de caracteres do nome <Quantidade>
Examples: 
	| nome  | Quantidade | Resultado           |
	| na    | 2          | permite criacao     |
	| teste | 5          | permite criacao     |
	| xxx   | 251        | permite criacao     |
	| yyy   | 250        | permite criacao     |

Scenario: Valida mensagem de detalhes para usuario
	When na tela de tarefas
	Then mensagem de detalhes e exibida
