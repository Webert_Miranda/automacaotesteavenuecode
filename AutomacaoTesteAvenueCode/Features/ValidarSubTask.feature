﻿Feature: ValidarSubTask
	Background: 
	 Given esteja logado na tela do AppTODo

Scenario: Valida existencia do comando de gerencia SubTask
	When possuo tarefa criada
	Then consigo acionar comando de gerenciar SubTask
	


Scenario: Criar SubTask
	When aciono comando Manage SubTasks
	And  abre popup de edicao da task
	And  escrevo o nome da SubTask no campo correto
	And  aciono como Add
	Then subTask criada com sucesso

Scenario Outline: Validar popup de edicao
	When  preencho os campos de informacao <Sub Task Description> <Due Data> <Status>
	And   aciono comando add
	Then  tenho <resultado> conform tabela abaixo

Examples: 
	| Description | Data     | Resultado                                 | Status  |
	| teste       | 12082018 | subtask criada com sucesso                | Aberto  |
	| teste1      | 12152019 | data invalida                             | Aberto  |
	|             | 20180812 | Descricao vazia e Task  nao criada criada | Aberto  |
	| teste2      | 20180812 | task criada com sucesso                   | Aberto  |
	| teste3      | 20180812 | subtask criado                            | fechado |
	| teste4      |          | subtask nao criada                        | Aberto  |  

Scenario: Validar alterar task principal a partir do modal
	When aciono o comando sub task description
	And  ter o modal aberto
	And  alterar o nome da task principal
	Then salvar

Scenario: Valida lista de sub task criadas
	When aciono o comando da sub taks description
	And  ter o modal aberto
	And  salvar
	Then sub task criada com sucesso
	And  aparece na lista de sub tasks


