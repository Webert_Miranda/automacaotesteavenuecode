﻿using AutomacaoTesteAvenueCode.Bases;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomacaoTesteAvenueCode.Pages
{
    class InicialPage : Class1
    {
        #region

        [FindsBy(How = How.XPath, Using = "//body/div/div/div/form/div[2]/input")]
        IWebElement icoLogin;

        [FindsBy(How = How.XPath, Using = "//body/div/div/div/form/div[3]/input")]
        IWebElement icoSenha;

        [FindsBy(How = How.XPath, Using = "//body/div/div/div/form/input")]
        IWebElement icoBotao;
        [FindsBy(How = How.XPath, Using = "//a[@href='/tasks']")]
        IWebElement icoMyTask;
        #endregion

        #region Actions

        public void Acessar()
        {
            icoLogin.SendKeys("webert.araujo@gmail.com");
        }

        public void Senha()
        {
            icoSenha.SendKeys("avenuecodeweberttestes2018");
           
        }

        public void GoPage()
        {
            icoBotao.Click();
            icoMyTask.Click();
        }
        #endregion
    }
}
