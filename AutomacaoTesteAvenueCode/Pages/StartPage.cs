﻿using AutomacaoTesteAvenueCode.Bases;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomacaoTesteAvenueCode.Pages
{
    class ValidaButtonAdd : Class1
    {
        #region Mapping tela
        [FindsBy(How = How.XPath, Using = "//a[@href='/tasks']")]
        IWebElement icoMyTask;

        [FindsBy(How = How.XPath, Using = "//h1[1]")]
        IWebElement icoInfoListUsuario;

        [FindsBy(How = How.XPath, Using = "//input[@id='new_task']")]
        IWebElement icoNewTask;

        [FindsBy(How = How.XPath, Using = "//body/div/div/div/form/div/span")]
        IWebElement icoSinalAdd;

        [FindsBy(How = How.XPath, Using = "//div[@class='bs - example']/div/table")]
        IWebElement icoTableTask;

        [FindsBy(How = How.XPath, Using = "//div[@class='bs - example']/div/table/tbody/tr/td[@class='col - md - 1']")]
        IWebElement icoDoneTask;

        [FindsBy(How = How.XPath, Using = "//div[@class='bs-example']/div/table/tbody/tr[1]/td[3]/input[@ng-model='task.public']")]
        IWebElement icoPublicTask;

        [FindsBy(How = How.XPath, Using = "//div[@class='bs-example']/div/table/tbody/tr[1]/td/button[@ng-click='editModal(task)']")]
        IWebElement icoManageSubTask;

        [FindsBy(How = How.XPath, Using = "//div[@class='bs-example']/div/table/tbody/tr[1]/td/button[@ng-click='removeTask(task)']")]
        IWebElement icoRemoveTask;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']")]
        IWebElement icoPopUpSubExiste;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div/form/textarea")]
        IWebElement icoNomeTaskModal;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div/div[@class='panel panel-default']/div[@class='panel-body']")]
        IWebElement icoAreaCriarSubTaskModal;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div/div[@class='panel panel-default']/div[@class='panel-body']/form/div/input")]
        IWebElement icoDescriptionSubTAsk;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div/div[@class='panel panel-default']/div[@class='panel-body']/form/div/p/input")]
        IWebElement icoDateSubTaskModal;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div/div[@class='panel panel-default']/div[@class='panel-body']/form/div/button")]
        IWebElement icoButtonAddSubTaskModal;

        [FindsBy(How = How.XPath, Using = "//body/div[@class='modal fade ng-isolate-scope in']/div/div/div/div/table/tbody/tr/td[@class='task_body col-md-8']/a")]
        IWebElement icoListSubTaskModal;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div/div[@ng-show='task.sub_tasks.length']/table/tbody/tr/td[@class='col-md-1']")]
        IWebElement icoDoneSubTaskModal;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div/div[@ng-show='task.sub_tasks.length']/table/tbody/tr[1]/td/button")]
        IWebElement icoRemoveSubTaskModal;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/div[@class='modal-footer ng-scope']/button")]
        IWebElement icoCloseModal;



        #endregion

        #region Actions of Page

        public Boolean ValidaLogado()
        {
            //String texto = icoInfoListUsuario.Text;
            return IsElementPresent(By.XPath("//h1[1]"));
        }

        public void ValidaNewTask()
        {
            icoMyTask.Click();
        }

        public void ValidaDigitarNewTask()
        {
            icoNewTask.SendKeys("Teste AvenueCode");
        }

        public void DigitaTaskTable()
        {

            //icoNewTask.FindElement(By.XPath("//input[@id='new_task']"));
            icoNewTask.SendKeys("teste");
        }

        public void ValidarAddNewTask()
        {
            icoSinalAdd.Click();
        }

        public Boolean ValidaQuantidadeDeCaracteres250()
        {
            icoNewTask.SendKeys("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            String texto = icoNewTask.Text;
            if (texto.Length <= 250)
            {
                return true;
            }
            else { return false; }

        }

        public Boolean ValidaQuantidadeDeCaracteres251()
        {
            icoNewTask.SendKeys("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            String texto = icoNewTask.Text;
            if (texto.Length > 250)
            {
                return false;
            }
            else { return true; }
        }

        public Boolean ValidaTableDeTasks()
        {
            return IsElementPresent(By.CssSelector("Teste AvenueCode"));
            
        }

        public Boolean ValidaInsercaoCaracteres250()
        {
            return IsElementPresent(By.XPath("//body/div/div/div/div/table/tbody/tr"));
        }

        public Boolean ValidaInsercaoCaracteres251()
        {
            return IsElementPresent(By.XPath("//body/div/div/div/div/table/tbody/tr"));
        }

        public void ValidaComandoDoneDaTask()
        {
            icoDoneTask.Click();
        }

        public void ValidaComandoPublicDaTask()
        {
            icoPublicTask.Click();
        }

        public void ValidaComandoManagerSubTask()
        {
            icoManageSubTask.Click();
        }

        public void ValidaComandoRemoveDaTask()
        {
            icoRemoveTask.Click();
        }

        public Boolean ValidaPopUpVisivel()
        {
            return icoPopUpSubExiste.Enabled; 
        }

        public void ValidaNomeTaskModal()
        {
            icoNomeTaskModal.Clear();
            icoNomeTaskModal.SendKeys("teste mudou");
        }

        public Boolean ValidaAreaCriarSub()
        {
            return icoAreaCriarSubTaskModal.Enabled;
        }

        public void ValidaDescriptionSubTask()
        {

            icoManageSubTask.Click();
            icoDescriptionSubTAsk.SendKeys("Teste do Sub");
            icoDateSubTaskModal.Clear();
            icoDateSubTaskModal.SendKeys("18/12/2018");
            
            /*icoDescriptionSubTAsk.SendKeys(" ");
            icoDateSubTaskModal.SendKeys("18/12/2018");
            icoButtonAddSubTaskModal();
            icoDescriptionSubTAsk.SendKeys("teste1");
            icoDateSubTaskModal.SendKeys("18/12/2018");
            icoButtonAddSubTaskModal();
            icoDescriptionSubTAsk.SendKeys("teste2");
            icoDateSubTaskModal.SendKeys("18/12/2018");
            icoButtonAddSubTaskModal();*/

        }

        public void ValidaDateSubTask()
        {
            icoDescriptionSubTAsk.SendKeys("Teste do Sub");
            icoDateSubTaskModal.SendKeys("18/12/2018");
           
            icoDescriptionSubTAsk.SendKeys(" teste ");
            icoDateSubTaskModal.SendKeys("18/13/2018");
           
            icoDescriptionSubTAsk.SendKeys("teste1");
            icoDateSubTaskModal.SendKeys("");
            
            icoDescriptionSubTAsk.SendKeys("teste2");
            icoDateSubTaskModal.SendKeys("18/12/2019");
           
        }

        public void ValidaAddSubTask()
        {
            icoButtonAddSubTaskModal.Click();
        }

        public Boolean ValidaListSubTask()
        {
            return IsElementPresent(By.CssSelector("Teste do Sub"));
        }

        public void ValidaDoneSubTask()
        {
            icoDoneSubTaskModal.Click();
        }

        public void ValidaRemoveSubTasj()
        {
            icoRemoveSubTaskModal.Click();
        }

        public void ValidaCloseSubTaskModal()
        {
            icoCloseModal.Click();
        }

        #endregion
    }
}
